import { Note } from '../models/note'

export const NOTES: Note[] = [{
        id: '1',
        name: 'B Note One',
        created: 1471294800000,
        lastModified: 1471294800000,
        content: `1 Lorem ipsum dolor sit amet, consectetur
                  adipisicing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit
                  in voluptate velit esse cillum dolore eu fugiat
                  nulla pariatur. Excepteur sint occaecat cupidatat
                  non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.`
    }, {
        id: '2',
        name: 'A Note Two',
        created: 1471208400000,
        lastModified: 1471381200000,
        content: `2 Lorem ipsum dolor sit amet, consectetur
                  adipisicing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit
                  in voluptate velit esse cillum dolore eu fugiat
                  nulla pariatur. Excepteur sint occaecat cupidatat
                  non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.`
    }, {
        id: '3',
        name: 'C Note Three',
        created: 1471381200000,
        lastModified: 1471467600000,
        content: `3 Lorem ipsum dolor sit amet, consectetur
                  adipisicing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit
                  in voluptate velit esse cillum dolore eu fugiat
                  nulla pariatur. Excepteur sint occaecat cupidatat
                  non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.`
    }
];
