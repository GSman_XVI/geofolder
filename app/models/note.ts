export class Note {
    id: string;
    name: string;
    created: number;
    lastModified: number;
    content: string;
    _token: string;
}
