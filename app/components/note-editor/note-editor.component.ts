import { Component, OnInit, ViewChild, ElementRef, Input, NgZone, Output, EventEmitter } from '@angular/core';
import { NgForm }    from '@angular/forms'
import { FORM_DIRECTIVES } from '@angular/forms';
import { TinyMCEDirective } from '../../directives/tinymce.directive';
import { Note } from '../../models/note';
declare var tinymce: any;

@Component({
    selector: 'note-editor',
    templateUrl: 'app/components/note-editor/note-editor.component.html',
    styleUrls: ['app/components/note-editor/note-editor.component.css'],
    directives: [TinyMCEDirective, FORM_DIRECTIVES]
})
export class NoteEditorComponent implements OnInit {
    text: string;
    @Input() note: Note;
    @ViewChild('content') content: ElementRef;
    @Output() textChanged = new EventEmitter();

    constructor(
        public ngZone: NgZone
    ) {}

    ngOnInit() {
        tinymce.init({
            selector: '.editor',
            schema: 'html5',
            setup: (ed) => {
                ed.on('keyup change', (ed, l) => {
                    this.ngZone.run(() => {
                        this.textChanged.emit(this.note.content);
                    });
                });
            }
        });
        //console.log(this.note);
    }

    // textChanged(text) {
    //     this.text = text;
    //     this.content.nativeElement.innerHTML = this.text;
    // }
}
