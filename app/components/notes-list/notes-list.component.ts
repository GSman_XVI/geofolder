import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NotesService } from '../../services/notes.service';
import { Note } from '../../models/note';
import { NotesSortPipe } from '../../pipes/notes-sort.pipe';

@Component({
    selector: 'notes-list',
    templateUrl: 'app/components/notes-list/notes-list.component.html',
    styleUrls: ['app/components/notes-list/notes-list.component.css'],
    pipes: [NotesSortPipe]
})
export class NotesListComponent implements OnInit {
    tab: string;
    notes: Note[];
    currentNote: Note;

    // @Output() noteChanged = new EventEmitter();

    constructor(
        private notesService: NotesService
    ) {
        this.tab = "name";
        this.notes = new Array<Note>();
        this.currentNote = new Note();
    }

    ngOnInit() {
        this.notesService.notes.subscribe(
            notes => {
                this.notes = notes;
                if (notes != null) {
                    this.currentNote = notes[0];
                }
                // this.noteChanged.emit(notes[0]);
            }
        );
        this.notesService.note.subscribe(
            note => {
                this.currentNote = note;
                // this.noteChanged.emit(note);
            }
        );
        this.notesService.getNotes();
    }

    changeTab(tab: string) {
        this.tab = tab;
    }

    changeCurrentNote(id: string) {
        this.notesService.getNote(id);
    }

    addNote() {
        let note = new Note;
        note.id = new Date().getTime().toString();
        note.name = 'New note';
        note.content = 'Text';
        note.created = new Date().getTime();
        note.lastModified = new Date().getTime();
        this.notesService.addNote(note);
    }
}
