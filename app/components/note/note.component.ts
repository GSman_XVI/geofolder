import { Component, AfterViewInit, AfterContentInit, OnInit, ViewChild, ElementRef, Input, NgZone } from '@angular/core';
import { FORM_DIRECTIVES } from '@angular/forms';
import { NotesService } from '../../services/notes.service';
import { NoteEditorComponent } from '../note-editor/note-editor.component';
import { Note } from '../../models/note';
declare var tinymce: any;

@Component({
    selector: 'note',
    templateUrl: 'app/components/note/note.component.html',
    styleUrls: ['app/components/note/note.component.css'],
    directives: [NoteEditorComponent, FORM_DIRECTIVES]
})
export class NoteComponent implements AfterViewInit , OnInit, AfterContentInit {
    @ViewChild('content') content: ElementRef;
    note: Note;
    lastNote: Note;
    isEdit: boolean;

    constructor(
        public ngZone: NgZone,
        private notesService: NotesService
    ) {
        this.isEdit = false;
        this.note = new Note();
        this.lastNote = new Note();
    }

    ngOnInit() {
        // this.notesService.note.subscribe(
        //     note => {
        //         this.note = note;
        //         this.isEdit = false;
        //         setTimeout(_=> this.content.nativeElement.innerHTML = this.note.content);
        //         if (tinymce.activeEditor) {
        //             setTimeout(_=> tinymce.activeEditor.setContent(this.note.content));
        //         }
        //         //setTimeout(_=> console.log(this.content.nativeElement));
        //         // this.content.nativeElement.innerHTML = this.note.content;
        //         // if (tinymce.activeEditor) {
        //         //     tinymce.activeEditor.setContent(this.note.content);
        //         // }
        //     }
        // );
        // setTimeout(_=> tinymce.init({
        //     selector: '.editor',
        //     schema: 'html5',
        //     setup: (ed) => {
        //         ed.on('keyup change', (ed, l) => {
        //             this.ngZone.run(() => {
        //                 this.note.content = tinymce.activeEditor.getContent();
        //                 setTimeout(_=> this.content.nativeElement.innerHTML = this.note.content);
        //             });
        //         });
        //     }
        // }));
        // setTimeout(_=> this.content.nativeElement.innerHTML = this.note.content);
    }

    ngAfterViewInit() {

    }

    ngAfterContentInit() {
        console.log(this.content.nativeElement);
        tinymce.init({
            selector: '.editor',
            schema: 'html5',
            setup: (ed) => {
                ed.on('keyup change', (ed, l) => {
                    this.ngZone.run(() => {
                        this.note.content = tinymce.activeEditor.getContent();
                        this.content.nativeElement.innerHTML = this.note.content;
                    });
                });
            }
        });
        // console.log('error');
        this.notesService.note.subscribe(
            note => {
                this.note = note;
                this.isEdit = false;
                this.content.nativeElement.innerHTML = this.note.content;
                if (tinymce.activeEditor && this.note.content != undefined) {
                    tinymce.activeEditor.setContent(this.note.content);
                    // if (this.note.content != undefined) {
                    //     tinymce.activeEditor.setContent(this.note.content);
                    // } else {
                    //     tinymce.activeEditor.setContent("Test");
                    // }
                }
            }
        );
        this.content.nativeElement.innerHTML = this.note.content;
    }

    editClick() {
        this.isEdit = !this.isEdit;
        // this.lastNote = new Note();
        // this.lastNote.id = this.note.id;
        // this.lastNote.name = this.note.name;
        // this.lastNote.created = this.note.created;
        // this.lastNote.lastModified = this.note.lastModified;
        // this.lastNote.content = this.note.content;

        // console.log(this.lastNote);
        // this.note.name = "Test";
        // console.log(this.lastNote);
        // if (this.isEdit) {
        // }
        if (!this.isEdit) {
            this.notesService.editNote(this.note);
        }
    }

    cancelClick() {
        // console.log(this.note);
        // console.log(this.lastNote);
        this.note = this.lastNote;
        this.isEdit = false;
        setTimeout(_=> this.content.nativeElement.innerHTML = this.lastNote.content);
    }

    deleteClick() {
        this.notesService.deleteNote(this.note.id);
        this.isEdit
    }
}
