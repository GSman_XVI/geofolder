import { Component, OnInit } from '@angular/core';
import { NotesListComponent } from '../notes-list/notes-list.component';
import { NoteComponent } from '../note/note.component';
import { NotesService } from '../../services/notes.service';
import { Note } from '../../models/note';

@Component({
    selector: 'notebook',
    templateUrl: 'app/components/notebook/notebook.component.html',
    styleUrls: ['app/components/notebook/notebook.component.css'],
    directives: [NotesListComponent, NoteComponent],
    providers: [NotesService]
})
export class NotebookComponent implements OnInit {
    //currentNote: Note;

    constructor() { }

    ngOnInit() { }

    // noteChanged(note) {
    //     this.currentNote = note;
    // }
}
