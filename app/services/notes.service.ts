import { Injectable } from '@angular/core';
import { Note } from '../models/note';
import { NOTES } from '../data/notes-data';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Http, Headers, Response} from '@angular/http';

@Injectable()
export class NotesService {
    notes: BehaviorSubject<any>;
    note: BehaviorSubject<any>;
    token: string;

    constructor(private http: Http) {
        //this.notes = new BehaviorSubject<any>(NOTES);
        //this.note = new BehaviorSubject<any>(NOTES[0]);
        this.notes = new BehaviorSubject<any>(new Array<Note>());
        this.note = new BehaviorSubject<any>(new Note());
        this.token = document.body.dataset['token'];
        console.log(this.token);
    }

    getNotes() {
        console.log('get');
        let data;
        this.http.get('/apps/notes/notes/list')
            .subscribe(list => {
                data = list.json().items;
                if (data.length == 0) {
                    let note = new Note;
                    note.name = 'New note';
                    note.content = 'Text';
                    this.addNote(note);
                } else {
                    this.notes.next(data);
                    this.note.next(data[0]);
                }
            });
    }

    getNote(id: string) {
        let data;
        this.http.get('/apps/notes/notes/get/' + id)
            .subscribe(note => {
                data = note.json().data;
                this.note.next(data);
            });
    }

    addNote(note: Note) {
        console.log('add');
        note._token = this.token;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http
            .post('/apps/notes/notes/store', note, { headers: headers })
            .subscribe(resp => {
                console.log(resp);
                this.getNotes();
            });
        // this.notes.getValue().push(note);
        // this.notes.next(this.notes.getValue());
        // this.note.next(NOTES.find((n => n.id == note.id)));
    }

    deleteNote(id: string) {
        console.log('delete');
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http
            .post('/apps/notes/notes/delete/' + id, { headers: headers })
            .subscribe(resp => {
                console.log(resp);
                this.getNotes();
            });
        // let index = this.notes.getValue().indexOf(NOTES.find((note => note.id == id)));
        // this.notes.getValue().splice(index, 1);
        // this.notes.next(this.notes.getValue());
        // this.note.next(this.notes.getValue()[0]);
    }

    editNote(note: Note) {
        console.log('edit');
        note._token = this.token;
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http
            .post('/apps/notes/notes/update/' + note.id, note, { headers: headers })
            .subscribe(resp => {
                console.log(resp);
                this.getNotes();
            });
    }
}
