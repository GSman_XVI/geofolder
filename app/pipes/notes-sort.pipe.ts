import { Pipe, PipeTransform } from '@angular/core';
import { Note } from '../models/note';

@Pipe({
    name: 'NotesSort'
})
export class NotesSortPipe implements PipeTransform {
    transform(notes: Array<Note>, field: string): Array<Note> {
        return sort(notes, field);
    }
}

function sort(array, field) {
    array.sort((a: Note, b: Note) => {
        if (a[field] < b[field]) {
            return -1;
        } else if (a[field] > b[field]) {
            return 1;
        } else {
            return 0;
        }
    });
    return array;
}
