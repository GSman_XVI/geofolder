import { Directive, OnInit, NgZone, Output, Input, EventEmitter } from '@angular/core';
declare var tinymce: any;

@Directive({
    inputs: ['tinymce'],
    selector: '[tinymce]'
})

export class TinyMCEDirective implements OnInit {
    text: string;
    @Output() textChanged = new EventEmitter();

    constructor(
        public ngZone: NgZone
    ) { }

    ngOnInit() {
        tinymce.init({
            selector: '.editor',
            schema: 'html5',
            setup: (ed) => {
                ed.on('keyup change', (ed, l) => {
                    this.ngZone.run(() => {
                        this.text = tinymce.activeEditor.getContent();
                        this.textChanged.emit(this.text);
                    });
                });
            }
        });
    }
}
