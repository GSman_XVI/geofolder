import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }     from '@angular/http';

import { NotebookComponent }  from './components/notebook/notebook.component';

@NgModule({
  imports: [ BrowserModule, HttpModule ],
  declarations: [ NotebookComponent ],
  bootstrap: [ NotebookComponent ]
})
export class AppModule { }
